# loaded-bootstrapify

Custom Shopify theme based on Bootstrap 4 and Gulp.

Requires npm, gulp, bower and Ruby Bundler installed.


## Installation

### 1. Clone the repo

`
git clone https://gitlab.com/GhostPirateBob/loaded-bootstrapify.git
`
### 2. Install dependencies

```sh
cd loaded-bootstrapify
bundle
npm install
bower install
```

### 3. Usage

To start watching the dev folder run

`gulp`

To build theme files for upload run

`
gulp build
`

That's it! 😁
